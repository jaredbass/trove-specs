Pod::Spec.new do |s|
  s.name = 'TroveClientSwift'
  s.homepage = 'https://bitbucket.org/jaredbass/trove-specs'
  s.summary = s.name
  s.ios.deployment_target = '9.0'
  s.osx.deployment_target = '10.11'
  s.tvos.deployment_target = '9.0'
  s.version = '0.0.19'
  s.source = { :git => 'https://jaredbass@bitbucket.org/jaredbass/trove-client-swift.git', :tag => s.version }
  s.authors = 'Justin Holbrook'
  s.license = {:type => 'Proprietary', :text => 'Proprietary'}
  s.source_files = 'SwaggerClient/Classes/**/*.swift'
  s.dependency 'Alamofire', '>= 4.5.0'
  s.swift_version = '4.0'
end
